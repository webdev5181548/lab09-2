import type { Type } from './Type'

type Product = {
  id?: number
  name: string
  price: number
  type: Type
  image: string
}
function getImageUrl(product: Product) {
  return `http://lpcalhost:3000/images/products/${product.image}`
}
export { type Product, getImageUrl }
